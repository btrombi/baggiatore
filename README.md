# README #

webapp per timbratura presenze. Proof of concept per valutare symfony vs framework minimale personale, e valutarne overhead.

### Tecnica ###

* Il Frontend utilizza bootstrap, mentre il backend implementatio tramite Symfony vs custom framework con obiettimo approccio minimalista.
* come db uso Sqlite  perché piu veloce di mysql e non necessita di configurazione utenza e permessi db, percui si versiona meglio.
* scambio dati codificato in Json nel body della POST
* Le api del backend esposte tramite rpc utilizzano parametri posizionali, invece che REST e  named parameters  (ovvero un dizionario).
questo  l'approccio posizionale é meno coumen ma é piú flessibile perche permette il passaggio dei named parametrs utilizzando il primo parametro un dizionario,
e consuma meno risorse, perché la codifica piú concisa, la valifdazione piú semplice (basta contare il numero dei parametri, non c'é il rischio che i nomi di paramtri non coincidano)
inoltre le api javascript e quelle del sistema operativo e la maggiornaza dei linguaggi di programmazione usano i parametri posizionali complicando l'integrazione.

* Per semplificare l'integrazione tra FE e BE una chiamata elenca le api esposte dal BE, via Javascript instanzio un oggetto chiamato BE che che comunica i dati al Backend. 
Consentendo di chiamare le api esposte dal BE come le normali api in JS, come propietá dell'oggetto BE, es per invocare la funzione timbraentrata del BE 
da frontend in js eseguo be.timbraentrata( idUtente,ora, cb ), i dati verranno inviati al BE, ed eseguita la callback al termine.
Con questa convenzione non bisogna di volta in volta specificare i parametri da scambiare, tutte le funzioni esposte dal BE sono disponibili automaticamente a FE, e l'autocompletamento del debugger mi consente di eseguire i metodi disponibili, diminuendo gli errori di battitura

### Set up ###

* ubuntu.18+apache+symfony+php7+sqlite+bootstrap
* composer create-project symfony/skeleton my_project_name
* composer require doctrine
* composer require symfony/serializer-pack
* clear ;symfony server:start --no-tls


### Api - pubbliche ###

* testDummy - ritorna un valore scolpito per test
* testMin - per test, ritorna il minimo tra i parametri, per verifica input e outpup
* testNoise - test esito funzione che sporca output, per verifica encoding risultante
* testException - test gestione eccezione
* exposed - ritorna funzioni disponibili, per integrazione Javascript
* subscribe - crea un uovo utente
* forgot - reinvia la password all'utente
* checkAuth - controlla username+password
* createRandomTimbratura - (funzione di test) solo per benchmark

### Api - basic http auth ###
* changepassword - (autenticata) cambia la password
* timbraentrata - (autenticata) timbra entrata
* timbrauscita - (autenticata) timbra uscita
* reportPersonale - (autenticata) per mostrare le proprie timbrature 
* reportGenerale - (autenticata)  per mostrare le timbrature di tutti gli utenti

### example via curl ###
* implementata cli, esempio ./backend.sh '["exposed"]'
* implementata cli, esempio ./backend.sh '["testMin",4,-75]'
* curl --header "Content-Type: application/json" --request POST --data '["exposed"]'  127.0.0.1:8000
* curl 'http://localhost:8000/' -X POST --data '["testMin",4,-75]'
* curl --header "Content-Type: application/json" --request POST --data '["createRandomTimbratura"]' 127.0.0.1:8000
* curl --header "Content-Type: application/json" --request POST --data '["changepassword","aaa"]'  127.0.0.1:8000 -u pippo:xyz
* curl --header "Content-Type: application/json" --request POST --data '["checkAuth"]'  127.0.0.1:8000 -u bruno.trombi@gmail.com:123
* curl --header "Content-Type: application/json" --request POST --data '["reportPersonale"]'  127.0.0.1:8000 -u bingo:123
* curl -s --header "Content-Type: application/json" --request POST --data '["timbraentrata"]'  127.0.0.1:8000 -u bingo:123
### Status ###

* In sviluppo, evitare utilizzo in produzione
* timbraUscita non aggiorna la tabella mostrata all'utente.
* manca maschera cambio password
* report generale non mostrato a FE
* custom tiny framework non ancora pubblicato
* benchmark non effettuato