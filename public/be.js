/*
questo codice é un eseprimento per esporre via javascript 
methodi diponibili lato backend via rpc.
andrebbe pesantemente rifattorizzato

urlBackend contiene l'url del BE server side

be.testDummy([],cbSuccess )
be.testMin([17,-31],cbSuccess )

*/


function backend( oData, cbSuccess) {

    var headers = new Headers({'content-type': 'application/json'})

    if(oData[0]=="checkAuth"){
        username= oData[1]
        password= oData[2]
    }
    
    if(username)
        headers.set('Authorization', 'Basic ' +btoa(username +":" +password) );

	var oConfig= { method: 'POST'
 		,headers: headers
 		,body:JSON.stringify(oData)
 	}
	fetch( urlBackend ,oConfig)
    .then(function(response) {
        return response.json()
    })
    .then(function(data) {
        cbSuccess( data )
    });
}


function backendInit(data){
    for(var iIndex in data){
    	var k= data[iIndex]
        //console.log("backendInit:",k)
        be[k] = baInitHelper.bind(undefined ,k ); //create a new function but, the firs parameter is value k
    }
}


function baInitHelper(sCb,data,cb){
    backend( [sCb].concat(data) ,cb ); //call the data
}

function cbSuccess(x){
	console.log("cbSuccess",x)
}


urlBackend= 'http://localhost:8000/'
be= {}

//initializaztion calling the discovery function
backend( ["exposed"],backendInit )


