<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TimbraturaRepository")
 */
class Timbratura
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="integer")
     */
    private $idUser;

    /**
     * @ORM\Column(type="datetime")
     */
    private $entrata;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $uscita;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdUser(): ?int
    {
        return $this->idUser;
    }

    public function setIdUser(int $idUser): self
    {
        $this->idUser = $idUser;

        return $this;
    }

    public function getEntrata(): ?\DateTimeInterface
    {
        return $this->entrata;
    }

    public function setEntrata(\DateTimeInterface $entrata): self
    {
        $this->entrata = $entrata;

        return $this;
    }

    public function getUscita(): ?\DateTimeInterface
    {
        return $this->uscita;
    }

    public function setUscita(?\DateTimeInterface $uscita): self
    {
        $this->uscita = $uscita;

        return $this;
    }


    public function dummy(): String
    {
        return "dummyOk:" .$this->idTimbratura ;
        //return $this;
    }
}
