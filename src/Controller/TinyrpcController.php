<?php
namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Timbratura;
use App\Entity\User;
use Symfony\Component\DependencyInjection\ContainerBuilder;


use Symfony\Component\Serializer\Encoder\JsonEncoder;



class TinyrpcController extends AbstractController{



    public function main(){
		$request = Request::createFromGlobals();
//		$path = $request->getPathInfo(); // the URI path being requested

		$args= json_decode( $request->getContent() ,true );
		$cmd= @array_shift( $args ); //extract the function name from the args
		//var_export($args);die();

		//$tmp = $containerBuilder->get('Api');
		global $aApi;
		global $exposed;
		$exposed= [];

		//whitelist funzioni che posso eseguire
		$oWhiteListFunctions= array( 
			'testDummy'=>function(){return "DUMMY.OK!"; }

			,"testMin"=>function($x,$y){return min($x,$y);}  
			
	
			,"testNoise"=>
				function(){
					echo "test noise!";
					return 453;
				}


			
			,"testException"=>
				function (){
				 	//throw new Exception('error');
					return 99;
				}



 			,"exposed"=>
				function (){
					global $aApi;
					return $aApi;
				}



 			,"subscribe"=>
				function ($name,$email,$password){
					$entityManager = $this->getDoctrine()->getManager();

					$item = new User();
			        $item->setName( $name );
			        $item->setEmail( $email );
			        $item->setPassword( $password );

			        // tell Doctrine you want to (eventually) save the Product (no queries yet)
			        $entityManager->persist($item);

			        // actually executes the queries (i.e. the INSERT query)
			        $entityManager->flush();

			        return $item->getId(); 
				}



 			,"forgot"=>
				function ($email){
					//passami il nome ed invio una mail alla casella registrata dall'utente indicato
					$entityManager = $this->getDoctrine()->getManager();
  					$item = $entityManager->getRepository('App:User')->findOneBy( array('email'=> $email) );
  					if(empty( $item )) return "email non trovata nel sistema!";
  					$name = $item->getName();
  					$password= $item->getPassword();
					return "dovrei inviare un email  to:$email with password: $password per il caro $name";
				}


 			,"checkAuth"=>
				function (){
					//controlla le credenziali fornite:name+password e ritorna 0 se non autenticato, oppure lo userId

					//piglia $name e $password dall'http.header.authorization
					global $request;
					$headerAuthorizationBasic= $request->headers->get('authorization');		
					$aDedcode= explode(":",base64_decode( substr($headerAuthorizationBasic,6) ) );

					if( count( $aDedcode )!=2) return 0;//numero di parametri errato, header non settato, o settato male, inutile proseguire 
					list($name, $password ) = $aDedcode;

					$entityManager = $this->getDoctrine()->getManager();
  					$item = $entityManager->getRepository('App:User')->findOneBy( array('name'=> $name, "password" => $password ) );

  					if(empty($item)) return 0;

					return $item->getId();						
				}


 			,"changepassword"=>
				function ($password= "aa"){
					global $exposed;
  					$idUser= $exposed["checkAuth"]();
  					//die("\nchangepassword:$idUser password:$password\n");
  					if( $idUser==0 ) return 0;
					$entityManager = $this->getDoctrine()->getManager();

  					$item = $entityManager->getRepository('App:User')->findOneBy( array('id'=> $idUser ) );

  					if(empty($item)) return 0;

					$item->setPassword( $password );

			        // tell Doctrine you want to (eventually) save the item (no queries yet)
			        $entityManager->persist($item);

			        // actually executes the queries (i.e. the INSERT query)
			        $entityManager->flush();

					return $idUser;					
				}



 			,"createRandomTimbratura"=>
 				//todo autenticare
				function (){ // crea timbratura random per benchmark
					$entityManager = $this->getDoctrine()->getManager();

					$item = new Timbratura();
			        $item->setIdUser( rand(1, 9999) );

			        $now = new \DateTime();
			        $item->setEntrata($now);
			        $item->setUscita(null);

			        // tell Doctrine you want to (eventually) save the Product (no queries yet)
			        $entityManager->persist($item);

			        // actually executes the queries (i.e. the INSERT query)
			        $entityManager->flush();

        			return 42;
				}

 			,"timbraentrata"=>
				function (){ //crea nuova timbratura entrata, con data e ora attuale
					$entityManager = $this->getDoctrine()->getManager();

  					global $exposed;
  					$idUser= $exposed["checkAuth"]();

					$item = new Timbratura();
			        $item->setIdUser($idUser);

  					//aggiorno data timbratura con quella attuale
			        $now = new \DateTime();
			        $item->setEntrata($now);

			        // tell Doctrine you want to (eventually) save the item (no queries yet)
			        $entityManager->persist($item);

			        // actually executes the queries (i.e. the INSERT query)
			        $entityManager->flush();

  					$tmp = $entityManager->getRepository('App:Timbratura')->findBy(
				            array('idUser'=> $idUser)
				            ,array('id' => 'ASC','entrata' => 'DESC' )	
					);

  					//retrieve use infos
  					$userRow = $entityManager->getRepository('App:User')->findOneBy( array('id'=> $idUser ) );
  					$nome=  $userRow->getName(); //"antani";
  					$email= $userRow->getEmail();//"X3";

					return [ $item->getId(),$nome,$email, $now->format('Y-m-d H:i:s'),"" ,0 ];
				}


 			,"timbrauscita"=>
 				//Timbra ultimo record
				function (){ //aggiorna timbratura uscita, con data e ora attuale
					$entityManager = $this->getDoctrine()->getManager();

  					global $exposed;
  					$idUser= $exposed["checkAuth"]();

  					$item = $entityManager->getRepository('App:Timbratura')->findOneBy(
			            array('idUser'=> $idUser ,'uscita'=>null) //
			            ,array('id' => 'DESC')	
					);

  					if(empty( $item )) return [];

			        $item->setIdUser($idUser);

  					//aggiorno data timbratura con quella attuale
			        $now = new \DateTime();
			        $item->setUscita($now);

			        // tell Doctrine you want to (eventually) save the item (no queries yet)
			        $entityManager->persist($item);

			        // actually executes the queries (i.e. the INSERT query)
			        $entityManager->flush();

  					//retrieve use infos
  					$userRow = $entityManager->getRepository('App:User')->findOneBy( array('id'=> $idUser ) );
  					$nome=  $userRow->getName(); //"antani";
  					$email= $userRow->getEmail();//"X3";

					$dateEntrata = $item->getEntrata() ;
					$dfEntrata = $dateEntrata->format('Y-m-d H:i:s');

					$minutesDiff= $now->diff($dateEntrata)->format('%i'); 

					return [ $item->getId(),$nome,$email,$dfEntrata ,$now->format('Y-m-d H:i:s'),$minutesDiff ];
				}


 			,"reportPersonale"=>
 				//TODO impedire report timbrature altrui
				function (){ //ritorna le propie timbrature
				
  					global $exposed;
  					$idUser= $exposed["checkAuth"]();

					$entityManager = $this->getDoctrine()->getManager();

  					$tmp = $entityManager->getRepository('App:Timbratura')->findBy(
				            array('idUser'=> $idUser)
				            ,array('id' => 'ASC','entrata' => 'DESC' )	
					);

  					$item = $entityManager->getRepository('App:User')->findOneBy( array('id'=> $idUser ) );
  					$nome= $item->getName(); //"antani";
  					$email= $item->getEmail();//"X3";


  					$r= [];
  					foreach ($tmp as $item) {

						$dfEntrata= "";
  						$dateEntrata = $item->getEntrata() ;
  						if(!empty($dateEntrata))
  							$dfEntrata = $dateEntrata->format('Y-m-d H:i:s');

						$dfUscita= "";  						
						$dateUscita= $item->getUscita();
  						if(!empty($dateUscita))
  							$dfUscita= $dateUscita->format('Y-m-d H:i:s');

  						$minutesDiff= "";
  						if(!empty($dfUscita)&&!empty($dfEntrata) ){
  							$minutesDiff= $dateUscita->diff($dateEntrata)->format('%i'); 
  						}
  						$r[]= [ $item->getId(),$nome,$email, $dfEntrata,$dfUscita ,$minutesDiff ];
  					}
					return $r;
				}


 			,"reportGenerale"=>
				function (){ // ritorna le timbrature di tutti gli utenti ordinate per data uscita decrescente

					global $exposed;
  					$idUser= $exposed["checkAuth"]();
  					if( $idUser<10 ) return []; //solo i primi 10 utenti abilitati

					$entityManager = $this->getDoctrine()->getManager();

  					//findOneBy  findBy countBy
  					$tmp = $entityManager->getRepository('App:Timbratura')->findAll(
				            array()
				            ,array('uscita' => 'DESC','idUser' => 'DESC')	
					);

  					//serializza le entitá come array, di modo che venga poi encodato in json
				    $serializer = $this->get('serializer');
					return $serializer->normalize($tmp, null);
				}




 		);

//todo refactory, cosí é brutto brutto, ma funziona
$aApi= array_keys($oWhiteListFunctions);
$exposed= $oWhiteListFunctions;

		//se presente nella whitelist, la eseguo, oppure fine
		if( !array_key_exists( $cmd,$oWhiteListFunctions ) )
	       //return ( new JsonResponse( ['error' => "method not allowed" ] ,405 ) ) ;
			{$cmd= "reportPersonale";$args=[];}

		//require 
		//array_map( 'require',glob("custom/*.php"),  );

		//try to exec the requested function
	    try {// to execute
	        ob_start();//defend the return value against noisy function on stdout.
	        //so an ill implemented function cannot break the json output
	        $data= call_user_func_array( $oWhiteListFunctions[$cmd] ,$args );
	        $noise = ob_get_contents();
	        ob_end_clean();

	        if($noise)
	        	return ( 
	        		new JsonResponse( 
		        		[
		                    "dataReturned" =>$data
		                    ,"noise"=>$noise
		                    ,"error" => "json data broken, probably some data was printed on stdout"
		        		] 
		        		,502 
		        	) 
	    		);

	        return new JsonResponse( $data,200 );//happy end, all seems ok.
	    }


	    catch (Exception $e) {
	        $line= $e->getLine();
	       
			new JsonResponse( [ "line"=> $line
	                ,"getMessage"=> $e->getMessage()
	                ,"file"=> $e->getFile()
	                ,"trace"=> $e->getTrace()
	                ,"error"=> "exception triggered"
	            ] ,500 );
	    }

    }
}
